// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.autos;

// imports
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;
import frc.robot.utilities.CommandUtililty;

public class Auto4_Clean extends AutoRoutine {                                  // class declaration
    private AutoPath[] m_PathList;                                              // working path list
    private final static String m_Name = "Two Cube Clean";                      // name of the auto
    private final AutoPath[] m_BlueList = {                                     // blue path list 
        new AutoPath("paths/1_Blue_Two_Cube_Clean.wpilib.json"),
        new AutoPath("paths/2_Blue_Two_Cube_Clean.wpilib.json", CommandUtililty.autoGrab()),
    };
    public final AutoPath[] m_RedList = {                                       // red path list
        new AutoPath("paths/1_Red_Two_Cube_Clean.wpilib.json"),
        new AutoPath("paths/2_Red_Two_Cube_Clean.wpilib.json", CommandUtililty.autoGrab()),
    };

    public Auto4_Clean() {                                                      // constructor for Auto4_Clean
        super(m_Name);                                                          // sets the name of the auto
        if (DriverStation.getAlliance() == Alliance.Blue) {                     // sets the working path list to whichever path list corresponds with the current driver station alliance
            m_PathList = m_BlueList;
        } else if (DriverStation.getAlliance() == Alliance.Red) {
            m_PathList = m_RedList;
        } else {
            m_PathList = null;
        }

        addCommands(CommandUtililty.getLowCubeCommand());                       // adds low cube command to auto routine
        addCommands(m_PathList);                                                // adds paths to auto routine
        addCommands(CommandUtililty.release());                                 // adds low cube command to autoroutine
    }
}