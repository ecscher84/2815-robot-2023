// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.autos;

// imports
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import frc.robot.utilities.AutoPath;
import frc.robot.utilities.AutoRoutine;
import frc.robot.utilities.CommandUtililty;

public class Auto3_Bump extends AutoRoutine {                               // class declaration
    private AutoPath[] m_PathList;                                          // working path list
    private final static String m_Name = "High Cube Mobility Bump";         // name of the auto
    private final AutoPath[] m_BlueList = {                                 // blue path list
        new AutoPath("paths/1_Blue_Mobility_Bump.wpilib.json"),
    };
    public final AutoPath[] m_RedList = {                                   // red path list
        new AutoPath("paths/1_Red_Mobility_Bump.wpilib.json"),
    };

    public Auto3_Bump() {                                                   // constructor for Auto3_Bump
        super(m_Name);                                                      // sets the name of the auto
        if (DriverStation.getAlliance() == Alliance.Blue) {                 // sets the working path list to whichever path list corresponds with the current driver station alliance
            m_PathList = m_BlueList;
        } else if (DriverStation.getAlliance() == Alliance.Red) {
            m_PathList = m_RedList;
        } else {
            m_PathList = null;
        }

        addCommands(CommandUtililty.getMidCubeCommand());                   // adds mid cube command to auto routine
        addCommands(m_PathList);                                            // adds path to auto routine
    }
}
