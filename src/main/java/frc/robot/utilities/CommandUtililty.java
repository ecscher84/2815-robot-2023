// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.utilities;

// imports
import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.subsystems.ClawSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.LEDSubsystem;
import frc.robot.subsystems.TelevatorSubsystem;
import frc.robot.subsystems.WristSubsystem;

/*
 * THIS CLASS EXISTS TO PROVIDE A SINGLE PLACE WHERE
 * COMMANDS CAN BE CREATED, THIS WAY COMMANDS CAN USE
 * EACH SUBSYSTEM RATHER THAN JUST ONE AS IF THEY WERE
 * CREATED IN EACH SUBSYSTEM
*/

public final class CommandUtililty {                                                                                // class declaration
    private static final DriveSubsystem m_DriveSubsystem = DriveSubsystem.getInstance();                            // getting instances of all the subsystems
    private static final TelevatorSubsystem m_TelevatorSubsystem = TelevatorSubsystem.getInstance();
    private static final LEDSubsystem m_LEDSubsystem = LEDSubsystem.getInstance();
    private static final WristSubsystem m_WristSubsystem = WristSubsystem.getInstance();
    private static final ClawSubsystem m_ClawSubsystem = ClawSubsystem.getInstance();

    // sequential commands
    public static Command getMidCubeCommand() {                                                                     // command that runs at the beginning of some autos
        return new SequentialCommandGroup(                                                                          // start of sequence
            grab(),                                                                                                 // grab the cube
            new ParallelCommandGroup(                                                                               // starts a parallel command group
                translateTelevatorTo(                                                                               // move the televator all the way to the top
                    Constants.k_TelescopeMidNodeSetpoint,
                    Constants.k_ElevatorMidNodeSetpoint
                ),
                moveWrist(-Constants.k_AutoWristDownMovementPower).withTimeout(Constants.k_AutoWristMovementTime)   // moves the wrist down at specified power for specified amount of time
                .andThen(
                    stopWrist()
                )
            ).until(                                                                                                // the entire parallel command group stops when the televator reaches its desired position
                m_TelevatorSubsystem.atSetpoint(
                    Constants.k_TelescopeMidNodeSetpoint,
                    Constants.k_ElevatorMidNodeSetpoint
                )
            ),
            toggleClaw(),                                                                                           // release the cube
            new ParallelCommandGroup(                                                                               // starts a parallel command group
                translateTelevatorTo(                                                                               // move the televator all the way down
                    Constants.k_TelescopeLowNodeSetpoint,
                    Constants.k_ElevatorLowNodeSetpoint
                ),        
                moveWrist(Constants.k_AutoWristUpMovementPower).withTimeout(Constants.k_AutoWristMovementTime)      // moves the wrist down at specified power for specified amount of time
                .andThen(
                    stopWrist()
                )
            ).until(                                                                                                // the entire parallel command group stops when the televator reaches its desired position
                m_TelevatorSubsystem.atSetpoint(
                    Constants.k_TelescopeLowNodeSetpoint,
                    Constants.k_ElevatorLowNodeSetpoint
                )
            )
        );
    }

    public static Command getLowCubeCommand() {                                                                     // command that runs at the beginning of some autos
        return new SequentialCommandGroup(                                                                          // start of sequence
            grab(),    
            moveWrist(-Constants.k_AutoWristLowNodeMovementPower).withTimeout(Constants.k_AutoWristLowMovementTime),// moves the wrist down at specified power for specified amount of time
            stopWrist(),                                                                                            // stop wrist
            release(),                                                                                              // release claw
            moveWrist(Constants.k_AutoWristLowNodeMovementPower).withTimeout(Constants.k_AutoWristLowMovementTime), // moves the wrist down at specified power for specified amount of time
            stopWrist()                                                                                             // stop wrist
        );
    }


    // drive commands
    public static Command driveCommand(DoubleSupplier forward, DoubleSupplier turn) {                               // command to drive the robot with the differential drive in drive subsystem
        return new RunCommand(
            () -> m_DriveSubsystem.arcadeDrive(
                Constants.Square(-forward.getAsDouble()),
                Constants.Square(-turn.getAsDouble())
            )
        );
    }

    public static Command driveCommand(double forward, double turn) {                                               // command to drive the robot with the differential drive in drive subsystem
        return new RunCommand(
            () -> m_DriveSubsystem.arcadeDrive(
                Constants.Square(forward),
                Constants.Square(turn)
            )
        );
    }

    public static Command setMaxSpeed(double speed) {                                                               // command to set the max drive train speed
        return new InstantCommand(
            () -> m_DriveSubsystem.setMaxSpeed(speed)
        );
    }

    // balance
    public static Command balanceCommand() {                                                                        // command to balance the robot
        return new RunCommand(
            () -> m_DriveSubsystem.balanceDrive(
                m_DriveSubsystem.getPitchController().calculate(
                    m_DriveSubsystem.getRobotPitch(),
                    0
                )
            )
        ).until(
            () -> m_DriveSubsystem.getPitchController().atSetpoint()
        );
    }

    // autonomous commands
    public static Command autoCommand(AutoRoutine auto) {                                                           // command to start the autonomous mode
        return new InstantCommand(
            () -> m_DriveSubsystem.resetOdometry(
                auto.getInitialPose()
            )
        )
        .andThen(
            auto
        ).andThen(balanceCommand());
    }

    public static Command zeroEncoders() {                                                                          // command to zero the televator encoders
        return new ParallelCommandGroup(
            new InstantCommand(
                () -> m_TelevatorSubsystem.reset()
            )
        );
    }

    // televator commands
    public static Command translateTelescope(double speed) {                                                        // command to move the telescope
        return new RunCommand(
            () -> m_TelevatorSubsystem.translateTelescope(speed)
        );
    }

    public static Command translateElevator(double speed) {                                                         // command to move the elevator
        return new RunCommand(
            () -> m_TelevatorSubsystem.translateElevator(speed)
        );
    }

    public static Command translateTelescopeTo(double position) {                                                   // command to move the telescope to a position
        return new RunCommand(
            () -> m_TelevatorSubsystem.translateTelescopeTo(position, 1)
        ).until(
            m_TelevatorSubsystem.atTelescopeSetpoint(position)
        );
    }

    public static Command translateElevatorTo(double position) {                                                    // command to move the elevator to a position
        return new RunCommand(
            () -> m_TelevatorSubsystem.translateElevatorTo(position, 1)
        ).until(
            m_TelevatorSubsystem.atElevatorSetpoint(position)
        );
    }

    public static Command translateTelevatorTo(double telescope, double elevator) {                                 // command to move the whole televator to a position
        return new ParallelCommandGroup(
            new RunCommand(
                () -> m_TelevatorSubsystem.translateTelescopeTo(telescope, 0.65)
            ),
            new RunCommand(
                () -> m_TelevatorSubsystem.translateElevatorTo(elevator, 1)
            )
        );
    }

    public static Command stopTelevator() {                                                                         // command to stop the televator
        return new InstantCommand(
            () -> m_TelevatorSubsystem.stopTelevator()
        );
    }

    // Wrist Commands
    public static Command moveWrist(double speed) {                                                                 // command to run the wrist
        return new RunCommand(
            () -> m_WristSubsystem.moveWrist(speed)
        );
    }

    public static Command stopWrist() {                                                                             // command to stop the wrist
        return new InstantCommand(
            () -> m_WristSubsystem.stopWrist()
        );
    }

    // claw commands
    public static Command toggleClaw() {                                                                            // command to toggle the claw
        return new InstantCommand(
            () -> m_ClawSubsystem.toggle()
        );
    }

    public static Command grab() {                                                                                  // command to toggle the claw
        return new InstantCommand(
            () -> m_ClawSubsystem.grab()
        );
    }

    public static Command autoGrab() {                                                                              // command to toggle the claw on the switch
        return new RunCommand(
            () -> m_ClawSubsystem.autoGrab()
        ).until(
            () -> m_ClawSubsystem.getState()
        );
    }

    public static Command release() {                                                                               // command to toggle the claw
        return new InstantCommand(
            () -> m_ClawSubsystem.release()
        );
    }

    // modes
    public static Command coneMode() {                                                                              // command to set the yellow mode of the LEDs
        return new ParallelCommandGroup(
            new InstantCommand(() -> m_LEDSubsystem.setYellow())
        );
    }

    public static Command cubeMode() {                                                                              // command to set the purple mode of the LEDs
        return new ParallelCommandGroup(
            new InstantCommand(() -> m_LEDSubsystem.setPurple())
        );
    }
}
