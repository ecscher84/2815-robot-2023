// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot;

// imports
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.autos.Auto1;
import frc.robot.autos.Auto2;
import frc.robot.autos.Auto3_Bump;
import frc.robot.autos.Auto3_Clean;
import frc.robot.autos.Auto4_Clean;
import frc.robot.utilities.AutoRoutine;
import frc.robot.utilities.CommandUtililty;

public class RobotContainer {                                                                                       // declaring the robot container
    private static final AutoRoutine[] m_AutoList = {                                                               // setting the auto list
        new Auto1(),
        new Auto2(),
        new Auto3_Bump(),
        new Auto3_Clean(),
        new Auto4_Clean()
    };
    
    private static Field2d m_Field = new Field2d();                                                                 // creating all the shuffleboard elements
    private static SendableChooser<AutoRoutine> m_AutoChooser = new SendableChooser<AutoRoutine>();

    private final CommandXboxController m_Controller = new CommandXboxController(Constants.k_XboxControllerPort);   // creating all of the controllers
    private final CommandJoystick m_Joystick = new CommandJoystick(Constants.k_JoystickControllerPort);

    public RobotContainer() {                                                                                       // constructor for robot container
        m_AutoChooser.setDefaultOption(                                                                             // setting the default auto mode
            "Charging Station",
            new Auto1()
        );
        configureBindings();
        resetList();
        putData();
    }
    
    public static Field2d getField() {                                                                              // method to get the field2d object
        return m_Field;
    }

    public static SendableChooser<AutoRoutine> getChooser() {                                                       // method to get the chooser
        return m_AutoChooser;
    }

    public static AutoRoutine[] getAutoList() {                                                                     // method to get the list of auto modes
        return m_AutoList;
    }

    public static void resetList() {                                                                                // method to reset the auto list
        AutoRoutine[] autoList = {
            new Auto1(),
            new Auto2(),
            new Auto3_Bump(),
            new Auto3_Clean(),
            new Auto4_Clean()
        };
        for (AutoRoutine auto : autoList) {
            m_AutoChooser.addOption(auto.getName(), auto);
        }
        m_AutoChooser.setDefaultOption(
            "Charging Station",
            new Auto1()
        );
    }

    public static void putData() {                                                                                  // method to put data on the shuffleboard
        SmartDashboard.putData("Autonomous Mode", m_AutoChooser);
        SmartDashboard.putData("Field", m_Field);
    }

    private void configureBindings() {                                                                              // configuring button bindings

        m_Controller.x().onTrue(
            CommandUtililty.getLowCubeCommand()
        );

        // balance
        m_Controller.a().whileTrue(
            CommandUtililty.balanceCommand()
        ).whileFalse(
            new InstantCommand(
                () -> CommandUtililty.driveCommand(0, 0)
            )
        );

        // wrist
        m_Joystick.button(6).whileTrue(
            CommandUtililty.moveWrist(0.95)
        ).onFalse(
            CommandUtililty.stopWrist()
        );

        m_Joystick.button(5).whileTrue(
            CommandUtililty.moveWrist(-0.5)
        ).onFalse(
            CommandUtililty.stopWrist()
        );

        // modes
        m_Controller.leftStick().onTrue(
            CommandUtililty.cubeMode()
        );

        m_Controller.rightStick().onTrue(
            CommandUtililty.coneMode()
        );

        m_Controller.leftBumper().onTrue(
            CommandUtililty.setMaxSpeed(1)
        );

        m_Controller.rightBumper().onTrue(
            CommandUtililty.setMaxSpeed(0.25)
        );

        // claw
        m_Joystick.button(1).onTrue(
            CommandUtililty.toggleClaw()
        );

        // televator presets
        m_Joystick.button(12).whileTrue(
            CommandUtililty.translateTelevatorTo(
                Constants.k_TelescopeLowNodeSetpoint,
                Constants.k_ElevatorLowNodeSetpoint
            )
        ).whileFalse(
            CommandUtililty.stopTelevator()
        );

        m_Joystick.button(10).whileTrue(
            CommandUtililty.translateTelevatorTo(
                Constants.k_TelescopeMidNodeSetpoint,
                Constants.k_ElevatorMidNodeSetpoint
            )
        ).whileFalse(
            CommandUtililty.stopTelevator()
        );

        // Elevator and Telescope adjust
        m_Joystick.povUp().whileTrue(
            CommandUtililty.translateElevator(1)
        ).onFalse(
            CommandUtililty.translateElevator(0)
        );

        m_Joystick.povDown().whileTrue(
            CommandUtililty.translateElevator(-1)
        ).onFalse(
            CommandUtililty.translateElevator(0)
        );

        m_Joystick.button(3).whileTrue(
            CommandUtililty.translateTelescope(1)
        ).onFalse(
            CommandUtililty.translateTelescope(0)
        );

        m_Joystick.button(4).whileTrue(
            CommandUtililty.translateTelescope(-1)
        ).onFalse(
            CommandUtililty.translateTelescope(0)
        );

        // Drive Axes
        m_Controller.axisGreaterThan(1, 0).onTrue(
            CommandUtililty.driveCommand(
                () -> m_Controller.getLeftY(),
                () -> m_Controller.getRightX()
            )
        );

        m_Controller.axisLessThan(1, 0).onTrue(
            CommandUtililty.driveCommand(
                () -> m_Controller.getLeftY(),
                () -> m_Controller.getRightX()
            )
        );

        m_Controller.axisGreaterThan(4, 0).onTrue(
            CommandUtililty.driveCommand(
                () -> m_Controller.getLeftY(),
                () -> m_Controller.getRightX()
            )
        );

        m_Controller.axisLessThan(4, 0).onTrue(
            CommandUtililty.driveCommand(
                () -> m_Controller.getLeftY(),
                () -> m_Controller.getRightX()
            )
        );
    }

    public Command getAutonomousCommand() {                                                                                 // method to get the autonomous command
        return CommandUtililty.autoCommand(
            m_AutoChooser.getSelected()
        );
    }
}