// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.subsystems;

// imports
import java.util.function.BooleanSupplier;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class TelevatorSubsystem extends SubsystemBase {                                                                 // class declaration
    private CANSparkMax m_ElevatorLeader;                                                                               // declare motors
    private CANSparkMax m_ElevatorFollower;
    private CANSparkMax m_Telescope;

    private RelativeEncoder m_ElevatorEncoder;                                                                          // declare encoders
    private RelativeEncoder m_TelescopeEncoder;

    private static TelevatorSubsystem m_Instance;                                                                       // declare televator subsystem instance

    public TelevatorSubsystem() {                                                                                       // constructor for televator subsystem
        m_ElevatorLeader = new CANSparkMax(Constants.k_ElevatorLeaderID, MotorType.kBrushless);                         // instantiating motors
        m_ElevatorFollower = new CANSparkMax(Constants.k_ElevatorFollowerID, MotorType.kBrushless);
        m_Telescope = new CANSparkMax(Constants.k_TelescopeID, MotorType.kBrushless);

        m_Telescope.setIdleMode(IdleMode.kBrake);                                                                       // sets motors to brake mode
        m_ElevatorLeader.setIdleMode(IdleMode.kBrake);
        m_ElevatorFollower.setIdleMode(IdleMode.kBrake);

        m_ElevatorFollower.follow(m_ElevatorLeader, true);                                                      // set elevator follower to follow elevator leader

        m_ElevatorEncoder = m_ElevatorLeader.getEncoder();                                                              // instantiating encoders
        m_TelescopeEncoder = m_Telescope.getEncoder();
    }

    public void reset() {                                                                                               // method to reset the encoder positions
        m_TelescopeEncoder.setPosition(0);
        m_ElevatorEncoder.setPosition(0);
    }

    public void translateTelescope(double speed) {                                                                      // method to move the telescope
        if (m_TelescopeEncoder.getPosition() >= Constants.k_TelescopeMax && speed > 0) {                                // doesn't move if its at to the end and trying to go farther
            m_Telescope.set(0);
        } else if (m_TelescopeEncoder.getPosition() <= 0 && speed < 0) {                                                // doesn't move if it's at to the start and trying to go farther
            m_Telescope.set(0);
        } else if (m_TelescopeEncoder.getPosition() <= 10 && speed < 0) {                                               // moves slowly if it's close the the start and trying to go in
            m_Telescope.set(speed / 4.0);
        } else {
            m_Telescope.set(speed);
        }
    }

    public void translateElevator(double speed) {                                                                       // method to move the elevator
        if (m_ElevatorEncoder.getPosition() >= Constants.k_ElevatorMax && speed > 0) {                                  // doesn't move if the elevator is at the top and trying to go farther
            m_ElevatorLeader.set(0);
        } else if (m_ElevatorEncoder.getPosition() <= Constants.k_ElevatorMin && speed < 0) {                           // doesn't move if the elevator is at the bottom and trying to go farther
            m_ElevatorLeader.set(0);
        } else if (m_ElevatorEncoder.getPosition() <= Constants.k_ElevatorMin + 10 && speed < 0) {                      // moves slow if the elevator is close to the top and trying to go farther
            m_ElevatorLeader.set(speed / 4.0);
        } else if (m_ElevatorEncoder.getPosition() >= Constants.k_ElevatorMax - 10 && speed > 0) {                      // moves slow if the elevator is close to the bottom and trying to go farther
            m_ElevatorLeader.set(speed / 4.0);
        } else {
            m_ElevatorLeader.set(speed);
        }
    }

    public void translateTelescopeTo(double position, double speed) {                                                   // moves the telescope to a desired position with tolerance
        if ( m_TelescopeEncoder.getPosition() > (position + 10)) {
            translateTelescope(-speed);
        } else if (m_TelescopeEncoder.getPosition() < (position - 10)) {
            translateTelescope(speed);
        } else if (m_TelescopeEncoder.getPosition() > (position + 3)) {
            translateTelescope(-speed);
        } else if (m_TelescopeEncoder.getPosition() < (position - 3)) {
            translateTelescope(speed);
        } else {
            m_Telescope.set(0);
        }
    }

    public void translateElevatorTo(double position, double speed) {                                                    // moves the elevator to a desired position with tolerance
        if ( m_ElevatorEncoder.getPosition() > (position + 10)) {
            translateElevator(-speed);
        } else if (m_ElevatorEncoder.getPosition() < (position - 10)) {
            translateElevator(speed);
        } else if (m_ElevatorEncoder.getPosition() > (position + 3)) {
            translateElevator(-speed);
        } else if (m_ElevatorEncoder.getPosition() < (position - 3)) {
            translateElevator(speed);
        } else {
            m_ElevatorLeader.set(0);
        }
    }

    public double getElevatorPosition() {                                                                               // gets the position of the elevator
        return m_ElevatorEncoder.getPosition();
    }

    public double getTelescopePosition() {                                                                              // gets the position of the telescope
        return m_TelescopeEncoder.getPosition();
    }

    public void stopTelevator() {                                                                                       // stops all the televator motors
        m_ElevatorLeader.set(0);
        m_Telescope.set(0);
    }

    public BooleanSupplier atTelescopeSetpoint(double position) {                                                       // gets whether the telescope is at it's endpoints
        return () -> (getTelescopePosition() >= (position - 3) && getTelescopePosition() <= (position + 3));
    }

    public BooleanSupplier atElevatorSetpoint(double position) {                                                        // gets whether the elevator is at it's endpoints
        return () -> (getElevatorPosition() >= (position - 3) && getElevatorPosition() <= (position + 3));
    }

    public BooleanSupplier atSetpoint(double telescope, double elevator) {                                              // gets whether the full televator is at it's endpoints
        return () -> atTelescopeSetpoint(telescope).getAsBoolean() && atElevatorSetpoint(elevator).getAsBoolean();
    }


    public static TelevatorSubsystem getInstance() {                                                                    // method to get the instance of televator subsystem
        if (m_Instance == null) {
            m_Instance = new TelevatorSubsystem();
        }
        return m_Instance;
    }
}