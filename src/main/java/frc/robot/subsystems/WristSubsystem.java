// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.subsystems;

// imports
import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class WristSubsystem extends SubsystemBase {         // class declaration
    private Spark m_Wrist;                                  // declaring Spark for wrist

    private static WristSubsystem m_Instance;               // declaring instance of wrist subsystem

    public WristSubsystem() {                               // constructor for wrist subsystem
        m_Wrist = new Spark(Constants.k_WristPort);         // instantiating spark with proper pwm port
    }

    public void moveWrist(double speed) {                   // method to move the wrist
        m_Wrist.set(speed);
    }

    public void stopWrist() {                               // method to stop the wrist
        m_Wrist.set(0);
    }

    public static WristSubsystem getInstance() {            // method to get the instance of the wrist subsystem
        if (m_Instance == null) {
            m_Instance = new WristSubsystem();
        }
        return m_Instance;
    }
}