// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.subsystems;

// imports
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class LEDSubsystem extends SubsystemBase {                                                   // class declaration
    private AddressableLED m_LED;                                                                   // LED object
    private AddressableLEDBuffer m_Buffer;                                                          // LED buffer object

    private int m_RainbowFirstPixelHue = 0;                                                         // hue color value of the first LED in the string on startup

    private boolean m_Rainbow;                                                                      // boolean representing whether we want to display a rainbow wave

    private static LEDSubsystem m_Instance;                                                         // instance of the LED subsystem

    public LEDSubsystem() {                                                                         // constructor for LED subsystem
        m_LED = new AddressableLED(Constants.k_LEDPort);                                            // instantiating LEDs on a PWM port

        m_Buffer = new AddressableLEDBuffer(Constants.k_LEDs);                                      // setting the LED buffer to the length of the led string
        m_LED.setLength(m_Buffer.getLength());                                                      // setting the LED object to the length of the LED string

        m_Rainbow = true;                                                                           // initially, we want a rainbow wave
        setRainbow();                                                                               // setting the initial rainbow colors

        m_LED.setData(m_Buffer);                                                                    // displaying the colors
        m_LED.start();                                                                              // starting the LED cycle
    }

    @Override
    public void periodic() {                                                                        // overridden periodic method: runs once every 20 ms while the robot is on
        if (m_Rainbow) {                                                                            // constantly wave the rainbow if we want rainbow colors
            setRainbow();
        }
        setData();                                                                                  // sets the data to the LEDs
    }

    public void setYellow() {                                                                       // method to set the LEDs to yellow
        m_Rainbow = false;
        for (int i = 0; i < m_Buffer.getLength(); i++) {
            m_Buffer.setRGB(i, 255, 255, 0);
        }
    }

    public void setPurple() {                                                                       // method to set the LEDs to purple
        m_Rainbow = false;
        for (int i = 0; i < m_Buffer.getLength(); i++) {
            m_Buffer.setRGB(i, 255, 0, 255);
        }
    }

    public void setRainbow() {                                                                      // method to set the LEDs to their next color in the rainbow
        for (var i = 0; i < m_Buffer.getLength(); i++) {
            final var hue = (m_RainbowFirstPixelHue + (i * 180 / m_Buffer.getLength())) % 180;
            m_Buffer.setHSV(i, hue, 255, 128);
        }
        m_RainbowFirstPixelHue += 3;
        m_RainbowFirstPixelHue %= 180;
    }

    public void setData() {                                                                         // method to set the LEDs' colors
        m_LED.setData(m_Buffer);
    }

    public static LEDSubsystem getInstance() {                                                      // method to get the instance of the LED subsystem
        if (m_Instance == null) {
            m_Instance = new LEDSubsystem();
        }
        return m_Instance;
    }
}

