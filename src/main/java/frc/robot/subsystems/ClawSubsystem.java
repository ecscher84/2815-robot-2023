// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

// package declaration
package frc.robot.subsystems;


import edu.wpi.first.wpilibj.DigitalInput;
// imports
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ClawSubsystem extends SubsystemBase {                          // class declaration
    private DoubleSolenoid m_Claw;                                          // defining a double solenoid for the piston on the claw
    private DigitalInput m_LimitSwitch;                                     // defining limit switch on claw
    private static ClawSubsystem m_Instance;                                // defining the instance of the subsystem

    public ClawSubsystem() {                                                // constructor for the claw subsystem
        m_Claw = new DoubleSolenoid(                                        // instantiating the double solenoid 
            PneumaticsModuleType.REVPH,                                     // type of module (REV Pneumatics Hub)
            Constants.k_ClawForwardChannel,                                 // forward channel number
            Constants.k_ClawReverseChannel                                  // reverse channel number
        );
        m_Claw.set(Constants.k_InitialState);                               // sets the claw to it's initial state when enabled
        m_LimitSwitch = new DigitalInput(Constants.k_ClawDIOChannel);
    }

    public void release() {                                                 // grab method
        m_Claw.set(Value.kForward);                                         // set claw to close
    }

    public void grab() {                                                    // release method
        m_Claw.set(Value.kReverse);                                         // set claw to open
    }

    public void autoGrab() {                                                // auto grab method
        if (!m_LimitSwitch.get()) {                                          // if limit switch is depressed
            m_Claw.set(Value.kReverse);                                     // close the claw
        }
    }

    public boolean getState() {
        if (m_Claw.get() == Value.kReverse) {
            return true;
        }
        return false;
    }

    public void toggle() {                                                  // toggle method
        if (m_Claw.get() == Value.kOff) {                                   // set the claw to go forward if the claw is in the off state
            m_Claw.set(Value.kForward);
        } else {
            m_Claw.toggle();                                                // otherwise, toggle the claw to the opposite state
        }
    }

    public static ClawSubsystem getInstance() {                             // method to get the instance of the claw subsystem
        if (m_Instance == null) {
            m_Instance = new ClawSubsystem();
        }
        return m_Instance;
    }
}
