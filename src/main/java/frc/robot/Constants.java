// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;

public final class Constants {
    // Controller ports
    public static final int k_XboxControllerPort = 0;
    public static final int k_JoystickControllerPort = 1;
    
    // Actuator Constants
    public static final int k_LeftDriveLeaderID = 1;
    public static final int k_LeftDriveFollower1ID = 3;
    public static final int k_LeftDriveFollower2ID = 5;
    public static final int k_RightDriveLeaderID = 2;
    public static final int k_RightDriveFollower1ID = 4;
    public static final int k_RightDriveFollower2ID = 6;

    public static final int k_ElevatorLeaderID = 7;
    public static final int k_ElevatorFollowerID = 8;

    public static final int k_TelescopeID = 9;

    public static final int k_WristPort = 1;

    // LED Constants
    public static final int k_LEDPort = 0;
    public static final int k_LEDs = 19;
    
    // Trajectory Constants
    public static final double k_WheelDiameter = Units.inchesToMeters(6);
    public static final double k_GearBoxRatio = 7.31;
    public static final double k_PositionConversionFactor = (
        (1.0 / k_GearBoxRatio) * (Math.PI * k_WheelDiameter)
    );
    public static final double k_VelocityConversionFactor = (
        (1.0 / k_GearBoxRatio) * (Math.PI * k_WheelDiameter) * (1.0 / 60)
    );
    public static final double k_MaxSpeedMetersPerSecond = 2;
    public static final double k_MaxAccelerationMetersPerSecondSquared = 3;

    public static final double k_RamseteB = 2;
    public static final double k_RamseteZeta = 0.7;

    public static final double k_sVolts = 0.13447;
    public static final double k_vVoltSecondsPerMeter = 1.9603;
    public static final double k_aVoltSecondsSquaredPerMeter = 0.33796;

    public static final double k_pDriveVel = 0.001081;

    public static final double k_TrackwidthMeters = Units.inchesToMeters(19);
    public static final DifferentialDriveKinematics k_DriveKinematics = new DifferentialDriveKinematics(k_TrackwidthMeters);

    public static final DifferentialDriveVoltageConstraint k_AutoVoltageConstraint = new DifferentialDriveVoltageConstraint(
        new SimpleMotorFeedforward(
            Constants.k_sVolts,
            Constants.k_vVoltSecondsPerMeter,
            Constants.k_aVoltSecondsSquaredPerMeter),
        Constants.k_DriveKinematics,
        10
    );

    public static final TrajectoryConfig config = new TrajectoryConfig(
            Constants.k_MaxSpeedMetersPerSecond,
            Constants.k_MaxAccelerationMetersPerSecondSquared
        )
        .setKinematics(Constants.k_DriveKinematics)
        .addConstraint(Constants.k_AutoVoltageConstraint);

    // Pitch Constants
    public static final double k_pPitch = 0.05;
    public static final double k_iPitch = 0;
    public static final double k_dPitch = 0;

    // Elevator Constants
    public static final double k_ElevatorLowNodeSetpoint = 3;
    public static final double k_ElevatorMidNodeSetpoint = 180;
    public static final double k_ElevatorMax = 215;
    public static final double k_ElevatorMin = 3;

    // Telescope Constants
    public static final double k_TelescopeLowNodeSetpoint = 5;
    public static final double k_TelescopeMidNodeSetpoint = 31;
    public static final double k_TelescopeMax = 87.5;

    // Claw Constants
    public static final int k_ClawForwardChannel = 0;
    public static final int k_ClawReverseChannel = 1;

    public static final int k_ClawDIOChannel = 0;

    public static final Value k_InitialState = Value.kReverse;

    // Wrist Constants
    public static final double k_PWrist = 0.15;
    public static final double k_IWrist = 0;
    public static final double k_DWrist = 1;

    public static final double k_WristSetpoint = Math.PI;
    public static final double k_WristPositionConversionFactor = 1.7197;

    public static final double k_AutoWristMovementTime = 0.75;
    public static final double k_AutoWristDownMovementPower = 0.3;
    public static final double k_AutoWristUpMovementPower = 0.55;
    public static final double k_AutoWristLowMovementTime = 0.5;
    public static final double k_AutoWristLowNodeMovementPower = 1;


    // functions
    public static double Square(double b) {
        return Math.abs(b) * b;
    }
}